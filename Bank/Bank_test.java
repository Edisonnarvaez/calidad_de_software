package Bank;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
public class Bank_test {

    //maximo a transar 100.000
    @Test
    public void testConsignacion(){
        Transaccion transaccion = new Transaccion();
        Double resultadoTransaccion = transaccion.consignacion(1000000.1,0.0);
        Assertions.assertEquals(100000.1,resultadoTransaccion);
    }
    @Test
//El retiro debe ser inferior o igual al monto
    public void testRetiro(){
        Transaccion transaccion = new Transaccion();
        Double resultadoTransaccion = transaccion.retiro(1.0,0.0);
        Assertions.assertEquals(0.0,resultadoTransaccion);
    }


}
